import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditShopprofileComponent } from './edit-shopprofile.component';

describe('EditShopprofileComponent', () => {
  let component: EditShopprofileComponent;
  let fixture: ComponentFixture<EditShopprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditShopprofileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditShopprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

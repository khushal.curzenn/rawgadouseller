import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-tandc',
  templateUrl: './tandc.component.html',
  styleUrls: ['./tandc.component.css']
})
export class TandcComponent implements OnInit {
  tandc:any;
  tandcdata:any
  baseurl:any
  constructor(private seller:SellerserviceService, private http:HttpClient) { 
    this.baseurl = seller.baseapiurl2
  }

  ngOnInit(): void {
    this.http.get(this.baseurl +"api/common/get_page_from_slug/seller_web_privacy_policy" ).subscribe(res=>{
      this.tandc = res
      this.tandcdata = this.tandc.data
      console.log(this.tandcdata)
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.component.html',
  styleUrls: ['./add-bank.component.css']
})
export class AddBankComponent implements OnInit {
  registerform!: FormGroup
  baseurl:any;main_id:any;getstatus:any;main_email:any
  endAddress:any; endLatitude:any; endLongitude:any; mypickaddress:any

  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.main_email = localStorage.getItem('selleremail')
      this.registerform = new FormGroup({       
        bic:new FormControl('', Validators.required),
        iban:new FormControl('', Validators.required),
        bank_name: new FormControl('', Validators.required),
        account_number:new FormControl('', Validators.required),
          
       })
       this.mypickaddress = {

       }
     }
     

  ngOnInit(): void {
  }

  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    console.log( this.endAddress )
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    this.mypickaddress.address = this.endAddress
    this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  }

  sendBank(){
    this.registerform.value.seller_id = this.main_id
    this.registerform.value.email = this.main_email
    this.registerform.value.bank_address = this.mypickaddress

   
    if(this.registerform.valid){
      this.http.post(this.baseurl+"api/seller/updateBankDetail" , this.registerform.value ).subscribe(res=>{
        this.getstatus =res
        if(this.getstatus.status == true){
          // alert(this.getstatus.message)
          window.location.reload()
        }
      })
    }else{
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }
    
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EditShopprofileComponent } from '../edit-shopprofile/edit-shopprofile.component';
@Component({
  selector: 'app-seller-shop',
  templateUrl: './seller-shop.component.html',
  styleUrls: ['./seller-shop.component.css']
})
export class SellerShopComponent implements OnInit {
  mainid: any
  baseurl: any
  profile: any
  hosturl:any;

  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, public dialog: MatDialog) {
    this.mainid = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2;
    this.hosturl = seller.hosturl
  }

  ngOnInit(): void {
    this.http.get(this.baseurl + "api/seller/getuserprofile/" + this.mainid).subscribe(res => {
      this.profile = res
      console.log(this.profile)
    })
  }
  openDialog(){
    const dialogRef = this.dialog.open(EditShopprofileComponent ,{
      height: '600px',
      width: '700px',
      position: {
        top: '7vh',
        left: '30vw'
    },
    });
  }
  deleteAccount()
  {
    var result = confirm("Vouloir supprimer ?");
    if (result) {
        //Logic to delete the item
        //alert("yes delete");
        this.http.get(this.baseurl + "api/seller/deletevendor/" + this.mainid).subscribe(res => {
          this.profile = res
          //console.log(this.profile)
          if(this.profile.status == true)
          {
            localStorage.clear();
            window.location.href= this.hosturl+'/login'
          }
        })
    }else{
      //alert("not to delete");
    }
  }

}

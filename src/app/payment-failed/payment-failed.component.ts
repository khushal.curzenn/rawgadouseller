import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
@Component({
  selector: 'app-payment-failed',
  templateUrl: './payment-failed.component.html',
  styleUrls: ['./payment-failed.component.css']
})
export class PaymentFailedComponent implements OnInit {
  base_url: any; 
  constructor( private user: SellerserviceService) {
    this.base_url = user.baseapiurl2
   }
  ngOnInit(): void {
  }

}

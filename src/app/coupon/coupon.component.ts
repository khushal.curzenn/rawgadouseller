import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AddCouponComponent } from '../add-coupon/add-coupon.component';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent implements OnInit {
  main_id: any
  baseurl: any
  ordersarray: any
  orders: any;
  getStatus: any
  registerform !: FormGroup
  confirm: any
  myoptions: any;
  ordersarray2: any
  orders2: any;
  productsarray: any; isfrmChecked: any; checked: any
  mycouponid: any
  getSingleCouponsById: any; getSingleCouponsByIdRes: any; singleCoupon: any
  constructor(public dialog: MatDialog, private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.ordersarray = []
    this.orders = []
    this.productsarray = []
    this.getSingleCouponsById = this.baseurl + "api/seller/getSingleCouponsById/"
    this.registerform = new FormGroup({
      name: new FormControl('', Validators.required),
      code: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      provider_id: new FormControl(this.main_id),
      limit: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      monper: new FormControl('', Validators.required),

    })
    this.myoptions = {
      "seller_id": this.main_id,
      "violation": "",
      "instock": "",
      "removed": ""
    }
    this.mycouponid = ''

  }

  ngOnInit(): void {
    this.http.get(this.baseurl + "api/seller/getallcouponsbyvendorid/" + this.main_id).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data

    })
    this.http.post(this.baseurl + "api/product/getallproductsforseller", this.myoptions).subscribe(res => {
      this.ordersarray2 = res
      this.orders2 = this.ordersarray2.data
      console.log(this.orders, "this is")
    })
  }

  deleteCoupen(id: any) {

    var DelCoup = confirm("Voulez-vous supprimer ce coupon ? ")
    if (DelCoup == true) {
      this.http.get(this.baseurl + "api/seller/deletecoupen/" + id).subscribe(res => {

        this.getStatus = res
        if (this.getStatus.status == true) {

          window.location.reload()
        }

      })
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCouponComponent);
  }

  getCoupon(id: any) {
    console.log(id)
    this.mycouponid = id

    this.http.get(this.getSingleCouponsById + id).subscribe(res => {
      this.getSingleCouponsByIdRes = res
      if (this.getSingleCouponsByIdRes.status) {
        this.singleCoupon = this.getSingleCouponsByIdRes.data
        console.log(this.singleCoupon, " this.singleCoupon")

        this.registerform = new FormGroup({
          name: new FormControl(this.singleCoupon.name, Validators.required),
          code: new FormControl(this.singleCoupon.code, Validators.required),
          start_date: new FormControl(this.singleCoupon.start_date, Validators.required),
          end_date: new FormControl(this.singleCoupon.end_date, Validators.required),
          provider_id: new FormControl(this.main_id),
          limit: new FormControl(this.singleCoupon.limit, Validators.required),
          type: new FormControl(this.singleCoupon.type, Validators.required),
          monper: new FormControl(this.singleCoupon.discountamount, Validators.required),

        })
      }
    })
  }

  addCoupen() {
    this.registerform.value.products = this.productsarray
    this.registerform.value.id = this.mycouponid
    console.log(this.registerform.value)

    if (this.registerform.valid) {
      this.http.post(this.baseurl + "api/seller/saveorupdatecoupens", this.registerform.value).subscribe(res => {
        console.log(res)
        this.confirm = res
        if (this.confirm.status == true) {
          //alert("Coupen Added successs")
          window.location.reload();
        }
      })
    } else {
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }


  }
  productCheck(event: any, pro: any) {
    console.log(event.target.checked)

    if (event.target.checked) {
      this.productsarray.push(event.target.value)
    }
    else {
      let index = this.productsarray.indexOf(event.target.value);
      this.productsarray.splice(index, 1);
    }
    console.log(this.productsarray)

  }

}

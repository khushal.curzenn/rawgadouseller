import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-shop-review',
  templateUrl: './shop-review.component.html',
  styleUrls: ['./shop-review.component.css']
})
export class ShopReviewComponent implements OnInit {
  main_id:any
  baseurl:any
  ordersarray:any
  orders:any;
  myoptions:any
  searchText:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.main_id = localStorage.getItem('main_sellerid')
      this.baseurl = seller.baseapiurl2
      this.ordersarray =[]
      this.orders =[]
      this.myoptions = {
        product_name:"",
        user_name:"",
        review_time:"",
        rating:5,
        to_reply:"",
        replied:"",
        seller_id:this.main_id
    }
   
     }

  ngOnInit(): void {

    this.http.post(this.baseurl + "api/order/getshopratings", this.myoptions).subscribe(res=>{
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(this.orders)
    })
  }

}

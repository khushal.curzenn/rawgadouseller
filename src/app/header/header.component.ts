import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  main_id:any;
  token:any;email:any
  hosturl:any
  isOpen = true;
  baseurl: any;
  getSellerNotification:any; getSellerNotificationRes:any; mynotificatuons:any
  notificationCount:any
  constructor(private seller:SellerserviceService, private http: HttpClient) { 
    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.token = localStorage.getItem('token')
    this.email = localStorage.getItem('selleremail')
    this.hosturl = seller.hosturl
    this.getSellerNotification = this.baseurl+"api/common/getSellerNotificationCount/"+this.main_id
    this.notificationCount = 0

  }

  ngOnInit(): void {
    this.http.get( this.getSellerNotification).subscribe(res=>{
      this.getSellerNotificationRes = res
      if(this.getSellerNotificationRes.status){
        this.mynotificatuons = this.getSellerNotificationRes.all_count
        console.log(this.mynotificatuons, "this.mynotificatuons")
       // this.notificationCount = this.mynotificatuons.length
       
      }
    })
  }

  logout(){
    localStorage.clear();
    window.location.href= this.hosturl+'/login'
  }
  toggleNavbar(){
   
    this.isOpen = !this.isOpen;
   
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitMseegeComponent } from './wait-mseege.component';

describe('WaitMseegeComponent', () => {
  let component: WaitMseegeComponent;
  let fixture: ComponentFixture<WaitMseegeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitMseegeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WaitMseegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables)

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  baseurl:any;main_id:any;token:any;hostUrl:any;
  last_login_time:any; 
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.token = localStorage.getItem('token')
      this.hostUrl = seller.hosturl
     }

  ngOnInit(): void {

    console.log( this.main_id, this.token )
    
    if(this.main_id == null || this.token == null){
      window.location.href=this.hostUrl+"login"
    }
    this.last_login_time = localStorage.getItem('last_login_time');
    console.log("this.last_login_time "+ this.last_login_time);
    console.log(" this.last_login_time dashboard page " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = this.hostUrl+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
        localStorage.clear();
        window.location.href = this.hostUrl+"login";
      }
    }
   
  }

}

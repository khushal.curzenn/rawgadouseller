import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SellerserviceService {

  // hosturl = "http://localhost:4201/"
  // baseapiurl = "http://localhost:9001/admin-panel/"
  // baseapiurl2 = "http://localhost:9001/"

   hosturl = "https://vendeur.rawgadou.com/"
   baseapiurl = "https://mainserver.rawgadou.com/admin-panel/"
   baseapiurl2 = "https://mainserver.rawgadou.com/"
  main_id: any; token: any; email: any; sessioin: any
  last_login_time:any; mainid:any;
  constructor() {
    this.main_id = localStorage.getItem('main_sellerid')
    this.token = localStorage.getItem('token')
    this.email = localStorage.getItem('selleremail')
    this.sessioin = localStorage.getItem('loginsession')

    if (this.sessioin >= '24') {
      setTimeout(() => {
        localStorage.clear()
        alert("Your session timeout")
        //window.location.reload()
      }, 1000 * 60 * 60 * 24);
    }
    /*
    this.mainid = this.main_id;
    this.last_login_time = localStorage.getItem('last_login_time');
    console.log("this.last_login_time "+ this.last_login_time);
    console.log(" this.mainid " , this.mainid);

    if(this.mainid == "" || this.mainid == null || this.mainid == undefined ||  this.mainid == 'null' || this.mainid == 'undefined')
    {
      window.location.href = this.hosturl+"login";
    }

    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = this.hosturl+"login";
    }else{
      const date = new Date();
      //console.log("this.base_url"+this.base_url);
      var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
      console.log("hours "+hours);
      if(hours > 24)
      {
        localStorage.clear();
        window.location.href = this.hosturl+"login";
      }
    }

    */
  }
}

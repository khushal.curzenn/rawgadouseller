import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-about-oblack',
  templateUrl: './about-oblack.component.html',
  styleUrls: ['./about-oblack.component.css']
})
export class AboutOblackComponent implements OnInit {
  about:any;
  aboutdata:any
  baseurl:any
  constructor(private seller:SellerserviceService, private http:HttpClient) {
    this.baseurl = seller.baseapiurl2
   }

  ngOnInit(): void {
    this.http.get(this.baseurl +"api/common/get_page_from_slug/seller_web_about_us" ).subscribe(res=>{
      this.about = res
      this.aboutdata = this.about.data
     
    })
  }

}

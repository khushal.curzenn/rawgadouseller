import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ShipmentComponent } from './shipment/shipment.component';
import { MyordersComponent } from './myorders/myorders.component';
import { MyproductsComponent } from './myproducts/myproducts.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyincomeComponent } from './myincome/myincome.component';
import { MyBalanceComponent } from './my-balance/my-balance.component';
import { BankaccountComponent } from './bankaccount/bankaccount.component';
import { ShopReviewComponent } from './shop-review/shop-review.component';
import { ShopProfileComponent } from './shop-profile/shop-profile.component';
import { RegisterComponent } from './register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OtpComponent } from './otp/otp.component';
import { SellerRegisterComponent } from './seller-register/seller-register.component';
import { CouponComponent } from './coupon/coupon.component';
import { MessegesComponent } from './messeges/messeges.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import { AddCouponComponent } from './add-coupon/add-coupon.component';
import { AddBankComponent } from './add-bank/add-bank.component';
import { AllDataComponent } from './all-data/all-data.component';
import { YourProfileComponent } from './your-profile/your-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { CategoriesComponent } from './categories/categories.component';
import { TestCompComponent } from './test-comp/test-comp.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ParametersComponent } from './parameters/parameters.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { GoogleMapsModule } from '@angular/google-maps';
import { ToastrModule } from 'ngx-toastr';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TandcComponent } from './tandc/tandc.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { WaitMseegeComponent } from './wait-mseege/wait-mseege.component';

import { TableFilterPipe } from './table-filter.pipe';
import { SellerShopComponent } from './seller-shop/seller-shop.component';
import { EditShopprofileComponent } from './edit-shopprofile/edit-shopprofile.component';
import { AboutOblackComponent } from './about-oblack/about-oblack.component';
import { YourPersonalComponent } from './your-personal/your-personal.component';
import { HomeNavComponent } from './home-nav/home-nav.component';
import { ConatctObalckComponent } from './conatct-obalck/conatct-obalck.component';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { AddVariationComponent } from './add-variation/add-variation.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ConfirmRegisterComponent } from './confirm-register/confirm-register.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { MySubscriptionPackComponent } from './my-subscription-pack/my-subscription-pack.component';
import { EditVariationComponent } from './edit-variation/edit-variation.component';
registerLocaleData(localeFr, 'fr');
//import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';


@NgModule({
  declarations: [
    AppComponent,
    TableFilterPipe,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    HeaderComponent,
    SidebarComponent,
    StatisticsComponent,
    ShipmentComponent,
    MyordersComponent,
    MyproductsComponent,
    AddProductComponent,
    MyincomeComponent,
    MyBalanceComponent,
    BankaccountComponent,
    ShopReviewComponent,
    ShopProfileComponent,
    RegisterComponent,
    OtpComponent,
    SellerRegisterComponent,
    CouponComponent,
    MessegesComponent,
    AddCouponComponent,
    AddBankComponent,
    AllDataComponent,
    YourProfileComponent,
    EditProfileComponent,
    CategoriesComponent,
    TestCompComponent,
    ChangePasswordComponent,
    NotificationsComponent,
    ParametersComponent,
    AboutUsComponent,
    ContactUsComponent,
    TandcComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    WaitMseegeComponent,
    SellerShopComponent,
    EditShopprofileComponent,
    AboutOblackComponent,
    YourPersonalComponent,
    HomeNavComponent,
    ConatctObalckComponent,
    AddVariationComponent,
    EditProductComponent,
    ConfirmRegisterComponent,
    OrderDetailComponent,
    SubscriptionComponent,
    PaymentConfirmComponent,
    WithdrawComponent,
    ProductDetailComponent,
    MySubscriptionPackComponent,
    EditVariationComponent,
    PaymentSuccessComponent,
    PaymentFailedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    Ng2SearchPipeModule,
    GoogleMapsModule,
    GooglePlaceModule,
    ToastrModule.forRoot(),
    MatInputModule,
    MatDatepickerModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    




  ],
  providers: [
    { provide: LOCALE_ID, useValue: "fr-FR" },
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},],
  bootstrap: [AppComponent],
  
})
export class AppModule { }


// { provide: LOCALE_ID, useValue: "fr-FR" },
// {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
// {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
// {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},],
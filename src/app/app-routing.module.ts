import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AllDataComponent } from './all-data/all-data.component';
import { BankaccountComponent } from './bankaccount/bankaccount.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CouponComponent } from './coupon/coupon.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MessegesComponent } from './messeges/messeges.component';
import { MyBalanceComponent } from './my-balance/my-balance.component';
import { MyincomeComponent } from './myincome/myincome.component';
import { MyordersComponent } from './myorders/myorders.component';
import { MyproductsComponent } from './myproducts/myproducts.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { OtpComponent } from './otp/otp.component';
import { ParametersComponent } from './parameters/parameters.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SellerRegisterComponent } from './seller-register/seller-register.component';
import { ShipmentComponent } from './shipment/shipment.component';
import { ShopProfileComponent } from './shop-profile/shop-profile.component';
import { ShopReviewComponent } from './shop-review/shop-review.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { TandcComponent } from './tandc/tandc.component';
import { TestCompComponent } from './test-comp/test-comp.component';
import { WaitMseegeComponent } from './wait-mseege/wait-mseege.component';
import { YourProfileComponent } from './your-profile/your-profile.component';
import { SellerShopComponent } from './seller-shop/seller-shop.component';
import { AboutOblackComponent } from './about-oblack/about-oblack.component';
import { YourPersonalComponent } from './your-personal/your-personal.component';
import { ConatctObalckComponent } from './conatct-obalck/conatct-obalck.component';
import { AddVariationComponent } from './add-variation/add-variation.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { MySubscriptionPackComponent } from './my-subscription-pack/my-subscription-pack.component';
import { EditVariationComponent } from './edit-variation/edit-variation.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';

const routes: Routes = [
 
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'otp/:id', component: OtpComponent },
  // { path: 'home', component: HomeComponent },
  { path: 'checkingTest', component: TestCompComponent },
  { path: 'about_us', component: AboutUsComponent },
  { path: 'contact_us', component: ContactUsComponent },
  { path: 'termsandconditions', component: TandcComponent },
  { path: 'your_personal', component: YourPersonalComponent },
  { path: 'forget_password', component: ForgetPasswordComponent },
  { path: 'seller_register/:id', component: SellerRegisterComponent },
  { path: 'subscription', component: SubscriptionComponent },
  { path: 'payment_confirm', component: PaymentConfirmComponent },
  { path: 'resetPassword/:id', component: ResetPasswordComponent },
  { path: 'messagewait', component: WaitMseegeComponent },
  { path: 'about_Oblack', component: AboutOblackComponent },
      { path: 'paymentSuccess', component: PaymentSuccessComponent  },
      { path: 'paymentFailed', component: PaymentFailedComponent  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: 'shipment', component: ShipmentComponent, outlet: 'seller' },
      { path: 'statistics', component: StatisticsComponent, outlet: 'seller' },
      { path: 'myorders', component: MyordersComponent, outlet: 'seller' },
      { path: 'myproducts', component: MyproductsComponent, outlet: 'seller' },
      { path: 'add_Product', component: AddProductComponent, outlet: 'seller' },
      { path: 'edit_Product', component: EditProductComponent, outlet: 'seller' },
      { path: 'product_detail', component: ProductDetailComponent, outlet: 'seller' },
      { path: 'add_Variation', component: AddVariationComponent, outlet: 'seller' },
      { path: 'edit_Variation', component: EditVariationComponent, outlet: 'seller' },
      { path: 'my_income', component: MyincomeComponent, outlet: 'seller' },
      { path: 'my_balance', component: MyBalanceComponent, outlet: 'seller' },
      { path: 'my_account', component: BankaccountComponent, outlet: 'seller' },
      { path: 'shop_review', component: ShopReviewComponent, outlet: 'seller' },
      { path: 'shop_profile', component: ShopProfileComponent, outlet: 'seller' },
      { path: 'shop_profile2', component: SellerShopComponent, outlet: 'seller' },
      { path: 'coupon', component: CouponComponent, outlet: 'seller' },
      { path: 'messeges', component: MessegesComponent, outlet: 'seller' },
      { path: 'all_data', component: AllDataComponent, outlet: 'seller' },
      { path: 'your_profile', component: YourProfileComponent, outlet: 'seller' },
      { path: 'notifications', component: NotificationsComponent, outlet: 'seller' },
      { path: 'parameters', component: ParametersComponent, outlet: 'seller' },
      { path: 'termsandconditions', component: TandcComponent, outlet: 'seller' },
      { path: 'about_Oblack', component: AboutOblackComponent, outlet: 'seller' },
      { path: 'contact_Oblack', component: ConatctObalckComponent, outlet: 'seller' },
      { path: 'your_personal', component: YourPersonalComponent, outlet: 'seller' },
      { path: 'order_detail', component: OrderDetailComponent, outlet: 'seller' },
      { path: 'my_sub_package', component: MySubscriptionPackComponent, outlet: 'seller' },
      { path: 'withdrawal', component: WithdrawComponent, outlet: 'seller' },
      { path: '', component: StatisticsComponent,outlet: 'seller' }
     
    ]
  },
  { path: '', component: HomeComponent, pathMatch: 'full' }

];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

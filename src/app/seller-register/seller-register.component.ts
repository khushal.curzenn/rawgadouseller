import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WaitMseegeComponent } from '../wait-mseege/wait-mseege.component';
import { ConfirmRegisterComponent } from '../confirm-register/confirm-register.component';

@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.component.html',
  styleUrls: ['./seller-register.component.css']
})
export class SellerRegisterComponent implements OnInit {
  interests: any
  isLinear: any
  baseurl: any
  getstatus: any
  mainid: any
  registerform !: FormGroup
  registerform1 !: FormGroup
  myfullname: any
  myemail: any
  mypassword: any; showmymodal = false
  messege: any
  endAddress: any; endLatitude: any; endLongitude: any; bankAddress: any
  mycountrycode: any; myphone: any; getSubscriptionUrl: any
  getSubscriptiondata: any; createSubscriptionoptions: any
  mainSubscriptionURL: any; mainSubAPIres: any; myFiles: string[] = [];
  myFilesf: string[] = []; myFilesb: string[] = [];
  mypickaddress: any
  Finalregisterform: any
  notindiviual: any
  forntimage: any; backimage: any; shopimage: any

  mystep: any
  pickadderr: any
  bankadderr: any

  getuserprofile: any; getuserprofileRes: any;singlePro:any

  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, private toastr: ToastrService
    , private dialog: MatDialog) {
    this.interests = []
    this.getSubscriptiondata = []
    this.mainid = this.activateroute.snapshot.params['id']

    this.baseurl = seller.baseapiurl2
    this.getSubscriptionUrl = this.baseurl + 'api/subs/getAllSubscriptions'
    this.mainSubscriptionURL = this.baseurl + 'api/subs/createSubscription'
    this.getuserprofile = this.baseurl + 'api/seller/getuserprofile/' + this.mainid
    this.notindiviual = true
    this.pickadderr = false
    this.bankadderr = false
    this.mypassword = ''
    this.myemail = ''
    this.myfullname = ''
    this.mycountrycode = ''
    this.myphone = ''

    this.http.get(this.getuserprofile).subscribe(res => {
      this.getuserprofileRes = res
      if (this.getuserprofileRes.status) {
        this.singlePro = this.getuserprofileRes.data
       
        this.mypassword =  ''
        this.myemail = this.singlePro.email
        this.myfullname = this.singlePro.fullname
        this.mycountrycode = this.singlePro.countrycode
        this.myphone = this.singlePro.phone
        
      
      }
    })
    this.registerform = new FormGroup({

      vendortype: new FormControl('', Validators.required),
      companyname: new FormControl('',),
      ICorPassport: new FormControl('', Validators.required),


      shopname: new FormControl('', Validators.required),
      shopdesc: new FormControl(''),

      email: new FormControl(this.myemail),
      countrycode: new FormControl(this.mycountrycode),
      phone: new FormControl(this.myphone),
      fullname: new FormControl(this.myfullname),
      bankAddress: new FormControl(''),

      address: new FormControl('', Validators.required ),
      bic: new FormControl('' ),
      iban: new FormControl('' ),
      bank_name: new FormControl('' ),
      account_name: new FormControl('' ),

      password: new FormControl(this.mypassword),


    })

   


    this.createSubscriptionoptions = {
      subscriber_name: this.myfullname,
      subscriber_email: this.myemail,
      provider_id: this.mainid
    }
   
    this.mypickaddress = {

    }
    this.mystep = ''
  }

  ngOnInit() {
    this.http.get(this.getSubscriptionUrl).subscribe(res => {
      this.getSubscriptiondata = res;
      if(this.getSubscriptiondata.length > 0)
      {
        let subpac = this.getSubscriptiondata[0];
         this.createSubscriptionoptions.name_of_package = subpac.package
        this.createSubscriptionoptions.duration = subpac.duration
        this.createSubscriptionoptions.package_id = subpac._id
      }
      console.log("getSubscriptiondata ",this.getSubscriptiondata)
      console.log("name_of_package ",this.createSubscriptionoptions.name_of_package)

      

    })
    //this.sendSubscription();





  }
  // handleAddressChange2(address2: any) {

  //   this.endAddress = address2.formatted_address
  //   this.endLatitude = address2.geometry.location.lat()
  //   this.endLongitude = address2.geometry.location.lng()
  //   this.mypickaddress.address = this.endAddress
  //   this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  // }
  handleAddressChange(bankAddress: any) {
    //this.bankAddress = bankAddress.formatted_address
  }
  sendSubscription() {
    
   
  }

  onFileUpdate(event: any) {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
    //  console.log(this.myFiles)

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.shopimage = reader.result
      //console.log(reader.result );
    };
  }
  onFileUpdatefront(event: any): void {

    this.myFilesf = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFilesf.push(event.target.files[i]);
    }
    // console.log(this.myFilesf)


    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.forntimage = reader.result
      //console.log(reader.result );
    };

  }

  onFileUpdateback(event: any) {
    this.myFilesb = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFilesb.push(event.target.files[i]);
    }
    // console.log(this.myFilesb)

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.backimage = reader.result
      //console.log(reader.result );
    };
  }

  register() {
   
    this.mypickaddress.address = this.registerform.value.address
    this.mypickaddress.latlong = [0, 0];
    this.bankAddress = this.registerform.value.bankAddress;
    //bankAddress
    //address
    this.registerform.value.pickupaddress = this.mypickaddress
    this.Finalregisterform = new FormData();
    this.Finalregisterform.append('vendortype', this.registerform.value.vendortype);
    this.Finalregisterform.append('ICorPassport', this.registerform.value.ICorPassport);
    this.Finalregisterform.append('companyname', this.registerform.value.companyname);
    this.Finalregisterform.append('shopname', this.registerform.value.shopname);
    this.Finalregisterform.append('shopdesc', this.registerform.value.shopdesc);
    for (var i = 0; i < this.myFiles.length; i++) {
      this.Finalregisterform.append("shopphoto", this.myFiles[i]);
    }
    for (var i = 0; i < this.myFilesf.length; i++) {
      this.Finalregisterform.append("front", this.myFilesf[i]);
    }
    for (var i = 0; i < this.myFilesb.length; i++) {
      this.Finalregisterform.append("back", this.myFilesb[i]);
    }
    this.Finalregisterform.append('fullname', this.myfullname);
    this.Finalregisterform.append('email', this.myemail);
    this.Finalregisterform.append('phone', this.myphone);
    this.Finalregisterform.append('countrycode', this.mycountrycode);
    this.Finalregisterform.append('pickupaddress', JSON.stringify(this.registerform.value.pickupaddress));
    this.Finalregisterform.append('bic', this.registerform.value.bic);
    this.Finalregisterform.append('iban', this.registerform.value.iban);
    this.Finalregisterform.append('bank_name', this.registerform.value.bank_name);
    this.Finalregisterform.append('account_name', this.registerform.value.account_name);
    this.Finalregisterform.append('password',  this.mypassword);
    this.Finalregisterform.append('id', this.mainid);
    this.Finalregisterform.append('web_reg_step', 4);
    this.Finalregisterform.append('is_registered', true);


   // this.Finalregisterform.append('bank_address', JSON.stringify(this.bankAddress));
   this.Finalregisterform.append('bank_address', this.bankAddress);



    //form valid
    if (this.registerform.valid == true) {
      this.createSubscriptionoptions.subscriber_name =  this.myfullname
      this.createSubscriptionoptions.subscriber_email = this.myemail
      this.http.post(this.mainSubscriptionURL, this.createSubscriptionoptions).subscribe(res => {
        this.mainSubAPIres = res
        if (this.mainSubAPIres.status == true) {
          this.http.post(this.baseurl + "api/seller/createseller", this.Finalregisterform).subscribe(res => {
            this.getstatus = res
            if (this.getstatus.status == true) {
              // this.toastr.success(this.getstatus.message, 'Success');
              localStorage.clear()
              // localStorage.setItem('main_sellerid', this.mainid); 
               console.log("sad");
               console.log("this.getstatus ", this.getstatus);
              // return ;
              if (this.createSubscriptionoptions.name_of_package == 'individual') {
                this.dialog.open(ConfirmRegisterComponent, {
                  width: '70vw',
                  position: {
                    top: '-500px',
                    left: '0'
                  }
                });


              } else {
                
                window.location.href = this.mainSubAPIres.url;

              }
            } else {
              // this.toastr.error(this.getstatus.errmessage, 'Error');
            }
          })



        } else {
          // this.toastr.error(this.mainSubAPIres.message, 'Error');
        }
      })
    }
    else {
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }
  }


  getEnterprise(eve: any) {
    this.notindiviual = false
  }
  getindivisual(eve: any) {
    this.notindiviual = true
  }

  firststep() {

    if (this.registerform.controls['ICorPassport'].status == "VALID" && this.registerform.controls['vendortype'].status == "VALID") {
      this.mystep = 'step-1'
    } else {
      this.registerform.controls['ICorPassport'].markAsTouched();
      this.registerform.controls['vendortype'].markAsTouched();
    }

  }
  secondstep() {
    
    // if (this.registerform.controls['shopname'].status == "VALID" && this.mypickaddress.address != undefined) {
    //   this.mystep = 'step-2'
    //   this.pickadderr = false
    // } else {
    //   this.registerform.controls['shopname'].markAsTouched();
    //   this.pickadderr = false
    // }
    
    if(this.registerform.value.address != "" && this.registerform.value.shopname != "" )
    {
      this.mystep = 'step-2';
    }else{
      this.registerform.controls['shopname'].markAsTouched();
      this.registerform.controls['address'].markAsTouched();
    }
  }
  thirdstep() {
    this.register();
    //this.mystep = 'step-3';

    // if (this.registerform.controls['bic'].status == "VALID" && this.registerform.controls['iban'].status == "VALID" &&
    //   this.registerform.controls['bank_name'].status == "VALID" && this.registerform.controls['account_name'].status == "VALID" && this.bankAddress != undefined) {
    //   this.mystep = 'step-3'
    //   this.bankadderr = false

    // } else {
    //   this.registerform.controls['bic'].markAsTouched();
    //   this.registerform.controls['iban'].markAsTouched();
    //   this.registerform.controls['bank_name'].markAsTouched();
    //   this.registerform.controls['account_name'].markAsTouched();

    //   this.bankadderr = false
    // }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-coupon',
  templateUrl: './add-coupon.component.html',
  styleUrls: ['./add-coupon.component.css']
})
export class AddCouponComponent implements OnInit {
  registerform !: FormGroup
  main_id: any
  baseurl: any
  confirm: any
  myoptions: any; ordersarray: any; orders: any
  productsarray: any; isfrmChecked: any; checked: any
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.productsarray = []

    this.registerform = new FormGroup({
      name: new FormControl('', Validators.required),
      code: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      provider_id: new FormControl(this.main_id),
      limit: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      monper: new FormControl('', Validators.required),

    })

    this.myoptions = {
      "seller_id": this.main_id,
      "violation": "",
      "instock": "",
      "removed": ""
    }


  }

  ngOnInit(): void {
    this.http.post(this.baseurl + "api/product/getallproductsforseller", this.myoptions).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(this.orders, "this is")
    })
  }
  addCoupen() {
    this.registerform.value.products = this.productsarray
    console.log(this.registerform.value)
    
    
    if (this.registerform.valid) {
      this.http.post(this.baseurl + "api/seller/saveorupdatecoupens", this.registerform.value).subscribe(res => {
        console.log(res)
        this.confirm = res
        if (this.confirm.status == true) {
          //alert("Coupen Added successs")
          window.location.reload();
        }
      })
    } else {
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }


  }
  productCheck(event: any, pro: any) {
    console.log(event.target.checked)

    if (event.target.checked) {
      this.productsarray.push(event.target.value)
    }
    else {
      let index = this.productsarray.indexOf(event.target.value);
      this.productsarray.splice(index, 1);
    }
    console.log(this.productsarray)

  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import {ToastrService} from 'ngx-toastr'
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {
  baseurl: any
  status :any
  registerform !: FormGroup
  mainid:any; myemail:any
  resendotp:any; otpoptions:any
  getuserprofile: any; getuserprofileRes: any;singlePro:any
 
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute:ActivatedRoute,public toastr: ToastrService) {
    this.baseurl = seller.baseapiurl2
   
    this.mainid = this.activateroute.snapshot.params['id']
    this.resendotp =  this.baseurl+"api/seller/resend_verificatio_email_code"
    this.getuserprofile = this.baseurl + 'api/seller/getuserprofile/' + this.mainid
    this.registerform = new FormGroup({
     
      code: new FormControl(''),

    })
    this.otpoptions={
      email:''
    }

  }

  ngOnInit(): void {
    this.http.get(this.getuserprofile).subscribe(res => {
      this.getuserprofileRes = res
      if (this.getuserprofileRes.status) {
        this.singlePro = this.getuserprofileRes.data
       
        this.myemail = this.singlePro.email
        
      
      }
    })

  }
  verify() {

    
    this.registerform.value.id = this.mainid
    console.log(this.registerform.value)
      this.http.post(this.baseurl+"api/seller/verifycode" ,  this.registerform.value).subscribe(res=>{
       
        this.status = res
        console.log(this.status.status)
        if(this.status.status == true){
          // this.toastr.success("La vérification est réussie","Success",{
          //   timeOut: 3000,
          // })
         // alert("La vérification est réussie")
          this.router.navigate(['seller_register/'+this.mainid])
        }
        else{
          // this.toastr.error("Le code n'est pas valide","Error",{
          //   timeOut: 3000,
          // })
         // alert("Le code n'est pas valide")
        }
      })
  }

  resendOTP(){
    this.otpoptions.email = this.singlePro.email

  
  this.http.post( this.resendotp, this.otpoptions).subscribe(res=>{
    this.status = res
    this.registerform.reset()
  })
  }

}

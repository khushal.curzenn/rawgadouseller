
import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {
  getSubscriptiondata:any
  baseurl:any; 
  getSubscriptionUrl:any
  constructor(private seller: SellerserviceService, private http: HttpClient,) { 
    this.baseurl = seller.baseapiurl2
    this.getSubscriptionUrl = this.baseurl + 'api/subs/getAllSubscriptions'
  }

  ngOnInit(): void {
    this.http.get(this.getSubscriptionUrl).subscribe(res => {
      this.getSubscriptiondata = res
    })
  }

  sendSubscription(sub:any){

  }

}

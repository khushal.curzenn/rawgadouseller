import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  submitSellerContactForm!:FormGroup;baseurl:any;aboutAPI:any
  Apiresposnse:any
  constructor(private seller: SellerserviceService, private http: HttpClient,) { 
    this.baseurl = seller.baseapiurl2
    this.aboutAPI =  this.baseurl+"api/seller/submitSellerContactForm"
    this.submitSellerContactForm = new FormGroup({
      email: new FormControl('', [ Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      full_name: new FormControl('',  Validators.required),
      phone: new FormControl('', [Validators.required]),
      message: new FormControl('',  Validators.required),

    })
  }

  ngOnInit(): void {
  }
  sendEmail(){
   
    if(this.submitSellerContactForm.valid ){
      console.log(this.submitSellerContactForm.value)

      this.http.post(this.aboutAPI,this.submitSellerContactForm.value ).subscribe(res=>{
        this.Apiresposnse =  res
        this.submitSellerContactForm.reset()
        console.log(this.Apiresposnse)
      })



    }else{
      for (const control of Object.keys(this.submitSellerContactForm.controls)) {
        this.submitSellerContactForm.controls[control].markAsTouched();
      }
      return;
    }
  }

}

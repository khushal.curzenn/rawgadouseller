var base = $("meta[name='data-base']").attr("content");
var csrf = $('meta[name="csrf-token"]').attr('content');

$(function () {
    $('#order_from').datetimepicker({
        format: 'L'
    });
    $('#order_to').datetimepicker({
        format: 'L'
    });
});

// Toggle
// var ClassName = {
//       CONTROL_SIDEBAR_ANIMATE: 'control-sidebar-animate',
//       CONTROL_SIDEBAR_OPEN: 'control-sidebar-open',
//       CONTROL_SIDEBAR_SLIDE: 'control-sidebar-slide-open',
// };
//  var ClassName = {
//       COLLAPSED: 'sidebar-collapse',
//       OPEN: 'sidebar-open',
//       CLOSED: 'sidebar-closed'
//     };